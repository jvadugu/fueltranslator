package com.sevenEleven.dispenser.util;
import java.util.*;
import java.text.*;

public	class ProtocolUtitlities
    {
        /// <summary>
        /// Calculates LRC from byte[] buffer from index to count bytes and returns value
        /// </summary>
        /// <param name="ByteBuffer">Buffer to process</param>
        /// <param name="Index">Index to start calclulation from</param>
        /// <param name="LengthOfBuffer">Length to calculate to</param>
        public static int CalculateLRC(byte[] ByteBuffer, int Index, int EndIndex)
        {
            if ((ByteBuffer == null) || (Index < 0) || (Index >= ByteBuffer.length) || (ByteBuffer.length <= EndIndex))
            {
				try {
					throw new Exception();
				} catch (Exception e) {
					e.printStackTrace();
				}
            }

            int lrc = 0;

            for (int i = Index; i <= EndIndex; i++)
                lrc ^= ByteBuffer[i];

            return (lrc);
        }

        /// <summary>
        /// Strip the null characters out of a message block.
        /// </summary>
        /// <param name="ByteBuffer">Byte buffer with message. Message is expected to start with STX/SOH, end with ETX LRC.</param>
        public static byte[] StripNulls(byte[] ByteBuffer)
        {
            int i;
            int numberOfNullsFound = 0;

            for (i = 1; i < ByteBuffer.length; i++) 
                if(ByteBuffer[i] == 0x00)
                    numberOfNullsFound++;

            if (numberOfNullsFound > 0)
            {
                // need to strip buffer of nulls
                byte[] newBuffer = new byte[ByteBuffer.length - numberOfNullsFound];
                int j = 0;

                    // do not touch the LRC -- whether we strip before LRC or after this is safest
                    // if the LRC is a null, or it's wrong... we can't help
                for (i = 0; i < ByteBuffer.length - 1; i++)
                {
                    if (ByteBuffer[i] != 0x00)
                    {
                        newBuffer[j] = ByteBuffer[i];
                        j++;
                    }
                }
                    // move whatever the lrc is into the new buffer
                newBuffer[newBuffer.length - 1] = ByteBuffer[ByteBuffer.length - 1];

                return (newBuffer);
            }
                // return existing buffer
            return (ByteBuffer);
        }

    	//===== Calculate Pump LRC
    	//  Sum right nibbles from <STX> thru <LRC_NEXT>
    	//  2's Complement the sum
    	//  LRC = Right nibble of sum 
    	public static byte CalculatePumpLRC( byte[] byteBuffer, int startPos, int endPos) {
    		
            if ((byteBuffer == null) ||	(startPos < 0) || (endPos >= byteBuffer.length) || (startPos > endPos))
            {
    			try {
    				throw new Exception();
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
            }

    		int lrc = 0;
    		for (int i = startPos; i <= endPos; i++) {
    			lrc += byteBuffer[i] & 0x0F;
    		}
    		lrc = ~lrc; // 1's complement
    		++lrc;		// 2's complement
    		
    		// Return only least significant nibble as LRC
    		lrc &= 0x0F;		
    		
    		return (byte) lrc;
    	}
    	
    	//===== Calculate DL
    	//  Modulo 16 of 2's complement, use resulting right nibble only
    	public static byte CalculatePumpDL( byte number ) {
    		
    		// Update Data Length (DL)
    		int dl = number;
    		dl = ~dl;
    		++dl;
    		dl = dl % 0xF;
       		// Use only least significant nibble as LRC
    		dl &= 0x0F;
    		
    		return (byte)dl;
    		
    	}

    	//===== Format message as a hex dump
    	//
    	public static String FormatHexDump( byte[] bytes, int length ) {
    		
    		String strHex = new String();
    		
    		if (length > bytes.length ) {
    			length = bytes.length;
    		}
    		
    		for (int index = 0; index < length; index++) {
    			strHex += (String.format("%02x ", bytes[index]));
            }
    		
    		return strHex;
    		
    	}
    	
    	public static String FormatIOLogPrefix( int pumpID, int numBytes, boolean outMsg) {
    		
    	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    		String ioMsg = String.format(sdfDate.format(new Date()) + " Pump(%d) %s %s %d bytes ", pumpID, (outMsg) ? "w" : "r", (outMsg) ? "-->" : " <---", numBytes);
    		
    		return ioMsg;
    	}
    	
    	//====================================================================
    	// takes input byte array and outputs the hex string representation
    	public static String ByteArrayToString(byte[] ba, int numbytes)
    	{
    		StringBuilder sb = new StringBuilder();
    		byte b;
    		for (int jj = 0; jj < numbytes; jj++) {
    			b = ba[jj];
    			sb.append(String.format("%02X ", b));
    		}
    			
    		return (sb.toString());
    	}

    	//====================================================================
    	// takes input byte array and outputs the ASCII char string representation
    	public static String ByteArrayToCharString(byte[] ba, int numbytes)
    	{
    		StringBuilder sb = new StringBuilder();
    		byte b;
    		for (int jj = 0; jj < numbytes; jj++) {
    			b = ba[jj];
    			sb.append(String.format("%c", b));
    		}
    		
    		return (sb.toString());
    	}

    	//====================================================================
    	// takes input byte array and outputs the hex string representation
    	public static String ByteArrayToString(byte[] ba)
    	{
    		StringBuilder sb = new StringBuilder();
    		for (byte b : ba) {
    			sb.append(String.format("%02X ", b));
    		}
    		
    		return (sb.toString());
    	}
 /*
        /// <summary>
        /// Resize supplied array to new size
        /// </summary>
        /// <param name="oldArray">Source array to resize</param>
        /// <param name="newSize">New size for this array</param>
        public static System.Array ResizeArray(System.Array oldArray, int newSize)
        {
            int oldSize = oldArray.Length;
            System.Type elementType = oldArray.GetType().GetElementType();
            System.Array newArray = System.Array.CreateInstance(elementType, newSize);
            int preserveLength = System.Math.Min(oldSize, newSize);

            if (preserveLength > 0)
                System.Array.Copy(oldArray, newArray, preserveLength);
            return newArray;
        }
*/
    	
    	public static byte[] formatResponseMsg(byte id, int pos, byte[] byteArr) {
    		byte[] byteArrRes = new byte[byteArr.length+3];
    		int i=0;
    		byteArrRes[i] = id;
    		i++;
    		byteArrRes[i] = (byte)pos;
    		i++;
    		byteArrRes[i] = (byte)byteArr.length;
    		i++;
    		System.arraycopy(byteArr, 0, byteArrRes, i, byteArr.length);
    		return byteArrRes;
    	}
    	
    	public static void updateDispenserState() {
    		
    	}
    	
    	public static int getByteToInt(byte ID) {
    		return (ID & (byte) 0xFF);
    	}

		public static String parsePresetAmt(String presetAmount) {
			if (presetAmount.length() == 5) {
				return presetAmount;
			} else if (presetAmount.length() < 5) {
				return presetAmount+"00";
			}
			
			return "07990";
			
		}
    }
