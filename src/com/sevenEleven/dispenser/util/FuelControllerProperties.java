package com.sevenEleven.dispenser.util;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FuelControllerProperties {
	
	private static Properties appProps = new Properties();
	private static FuelControllerProperties instance = new FuelControllerProperties();
	private static final Logger logger = LogManager.getLogger(FuelControllerProperties.class.getName());
	
	/**
	 * @return the appProps
	 */
	public static Properties getAppProps() {
		return appProps;
	}

	/**
	 * @param appProps the appProps to set
	 */
	public static void setAppProps(Properties appProps) {
		FuelControllerProperties.appProps = appProps;
	}

	/**
	 * @return the instance
	 */
	public static FuelControllerProperties getInstance() {
		return instance;
	}
	/**
	 * @param fileName the LoadProps 
	 */
	public void loadProperties(String fileName) {
		
		try {
			File file = new File(fileName);
			appProps.load(new FileInputStream(file));
		
			
		    
		}catch(Exception e) {
			logger.error("Unable to Load Properties to start the Application : "+e.getMessage());
		}
		
	}
	
	/**
	 * @return the String value
	 */
	public static String getAppPropsWithKey(String key) {
		String retVal = appProps.getProperty(key, "UNKNOWN");
		return retVal.equalsIgnoreCase("UNKNOWN")?key+".UNKNOWN":retVal;
	}
	
	public static String getAppPropsWithKey(String key, String defaultValue) {
		String retVal = appProps.getProperty(key, defaultValue);
		return retVal;
	}
	/**
	 * @return the String value
	 */
	public  void setAppProps(String key, String value) {
		 appProps.setProperty(key, value);
	}

	

}
