package com.sevenEleven.dispenser.util;

import java.io.File;

import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sevenEleven.dispenser.config.RecMessageQueue;
import com.sevenEleven.dispenser.config.SerialIOConfig;
import com.sevenEleven.dispenser.pump.GlobalProperties;
import com.sevenEleven.dispenser.pump.PumpPriceChangeMessage;

public class JNotifyDemo extends Thread {
	private static final Logger logger = LogManager.getLogger(JNotifyDemo.class.getName());

	static RecMessageQueue queueRec = RecMessageQueue.getInstance();
	FileAlterationObserver observer;

	FileAlterationMonitor monitor = new FileAlterationMonitor(1L);

	public JNotifyDemo() {

		observer = new FileAlterationObserver(FuelControllerProperties.getAppPropsWithKey("fileWatchDirectory"));
		FileAlterationListener listener = new FileAlterationListenerAdaptor() {

			@Override
			public void onFileChange(File file) {
				

				if (FuelControllerProperties.getAppPropsWithKey("pricesProperties").endsWith(file.getName().trim())) {
					

					RuntimeValues.PRICE_CHANGE_FLAG = true;

					byte tempByte = 100;

					PumpPriceChangeMessage msgInfChange = new PumpPriceChangeMessage(tempByte, tempByte, tempByte,
							"TEMP");

					queueRec.setMessage(msgInfChange, SerialIOConfig.PUMP_PORT_TYPE);

					;
				}

				if (FuelControllerProperties.getAppPropsWithKey("globalProperties").endsWith(file.getName().trim())) {

					logger.info("::::::::::::::globalProperties Changed::::::::::::");


					RuntimeValues.DEAL_MESSAGE = GlobalProperties.getDealMessage()[0];
				}
			}
		};
		observer.addListener(listener);
		monitor.addObserver(observer);

	}

	public void run() {

		try {

			monitor.start();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
