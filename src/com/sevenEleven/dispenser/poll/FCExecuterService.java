package com.sevenEleven.dispenser.poll;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FCExecuterService {
	ExecutorService executor = null;
	public static FCExecuterService instance = new FCExecuterService();
	private static final Logger logger = LogManager.getLogger(FCExecuterService.class.getName());
	private FCExecuterService() {
		setExecutor(Executors.newCachedThreadPool());
	}
	
	public static FCExecuterService getInstance() {
		return instance;
	}

	public ExecutorService getExecutor() {
		return executor;
	}

	public void setExecutor(ExecutorService executor) {
		this.executor = executor;
	}
	
	public static void executeTask(Thread task) throws Exception {
		logger.info("inside executeTask");
		instance.getExecutor().execute(task);
	}
	
	public static void executeTask(Runnable task) throws Exception {
		//logger.info("inside executeTask Runnable:");
		instance.getExecutor().execute(task);
	}
	
	

}
