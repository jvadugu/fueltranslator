package com.sevenEleven.dispenser.poll;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sevenEleven.TranslatorProxy.TranslatorServer;
//import com.sevenEleven.dispenser.Dispenser;
//import com.sevenEleven.dispenser.DispenserApp;
import com.sevenEleven.dispenser.config.RecMessageQueue;
import com.sevenEleven.dispenser.config.RecMessageQueueForApps;
import com.sevenEleven.dispenser.config.SerialIOConfig;
import com.sevenEleven.dispenser.config.SerialIOMsgUtil;
import com.sevenEleven.dispenser.crind.CrindMessage;
import com.sevenEleven.dispenser.message.ControlFlow;
import com.sevenEleven.dispenser.message.ControlFlowFactory;
import com.sevenEleven.dispenser.message.GilbMessageInf;
import com.sevenEleven.dispenser.pump.PumpEOTTotalsMessage;
import com.sevenEleven.dispenser.pump.PumpMessage;
import com.sevenEleven.dispenser.pump.PumpTransaction;
import com.sevenEleven.dispenser.util.Constants;
import com.sevenEleven.dispenser.util.FuelControllerProperties;
import com.sevenEleven.dispenser.util.ProtocolUtilities;

public class ParseReceiveMsgController extends Thread {
	RecMessageQueue recqueInstance = RecMessageQueue.getInstance();
	private Integer portType = -1;
	private static final Logger logger = LogManager.getLogger(ParseReceiveMsgController.class.getName());
	private boolean flag;

	public ParseReceiveMsgController() {
		this.setPortType(portType);
		setName("ReceiveMsgController");
		logger.info("Start message data key");
		flag = Boolean.parseBoolean(FuelControllerProperties.getAppPropsWithKey("pump.flow"));
	}

	public void run() {
		int retries = 5;

		while (true) {

			try {

				if (!recqueInstance.isEmpty(SerialIOConfig.CRIND_PORT_TYPE)) {
					try {
						GilbMessageInf msgInf = recqueInstance.getMessage(SerialIOConfig.CRIND_PORT_TYPE);
						logger.info("Reading message data");
						logger.info(ProtocolUtilities.FormatIOLogPrefix(msgInf.getID(), msgInf.getLength(), true)
								+ msgInf.getMessageName() + " | try " + retries + " | hex: " + msgInf.toHex());

						invokeCrindContoller(msgInf);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}

				if (!recqueInstance.isEmpty(SerialIOConfig.PUMP_PORT_TYPE)) {
					try {
						GilbMessageInf msgInf = recqueInstance.getMessage(SerialIOConfig.PUMP_PORT_TYPE);
						logger.info("Reading message data Pump");
						logger.info(ProtocolUtilities.FormatIOLogPrefix(msgInf.getID(), msgInf.getLength(), true)
								+ msgInf.getMessageName() + " | try " + retries + " | hex: " + msgInf.toHex());

						invokePumpContoller(msgInf);

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				if ((recqueInstance.isEmpty(SerialIOConfig.PUMP_PORT_TYPE))
						&& (recqueInstance.isEmpty(SerialIOConfig.CRIND_PORT_TYPE))) {
					Thread.sleep(5);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
	private void invokePumpContoller(GilbMessageInf msgInf) {

		byte[] data = msgInf.getBytes();
		if (data.length < 4) {
			logger.info(" Invalid message. Ignoring the message ");
		}

		int devId = data[3]; // Get Crind device of rcvd msg
		byte devId_b = data[3]; // Get Crind device of rcvd msg
		logger.info(data.length + " ::::  " + ProtocolUtilities.FormatHexDump(data, data.length) + " ::: "
				+ msgInf.getMessageName());
		ControlFlow pumpFlow;

		String pumpID = String.valueOf(data[0]);

		if (msgInf.getMessageName().equalsIgnoreCase("PRICE_CHANGE")) {
			pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
			pumpFlow.execute(msgInf.getID(), Constants.PRICE_CHANGE_EXTERNAL);
			return;
		}

		if (msgInf.getMessageName().equalsIgnoreCase("EOT_TOTALS_RESP")) {
			submitEOTPrintTask(msgInf, data, Constants.PRINT);
			

		} else {
			switch (devId) {
			case SerialIOMsgUtil.ONLINE:
				submitPumpcommandProcess(msgInf, data, Constants.ONLINE);
				break;
			case PumpMessage.PMP_STATUS_DATA_ERROR:
				// pumpFlow.execute(data[0], Constants.ONLINE);
				logger.info(" PMP_STATUS_DATA_ERROR " + Constants.PMP_STATUS_DATA_ERROR);
				break;
			case PumpMessage.PMP_STATUS_EVENT:
				// pumpFlow.execute(data[0], Constants.PMP_STATUS_EVENT);
				logger.info(" PMP_STATUS_EVENT " + Constants.PMP_STATUS_EVENT);
				break;
			case PumpMessage.PMP_STATUS_OFF:
				// pumpFlow.execute(data[0], Constants.PMP_STATUS_OFF);
				logger.info(" PMP_STATUS_OFF " + Constants.PMP_STATUS_OFF);
				logger.info(" PMP_STATUS_OFF  #$%#$%#$%#$%#$ " + PumpMessage.PMP_STATUS_OFF);
				break;
			case PumpMessage.PMP_STATUS_CALL:
				// pumpFlow.execute(data[0], Constants.PMP_STATUS_CALL);
				logger.info(" PMP_STATUS_CALL " + Constants.PMP_STATUS_CALL);
				break;
			case PumpMessage.PMP_STATUS_BUSY:
				// pumpFlow.execute(data[0], Constants.PMP_STATUS_BUSY);
				logger.info(" PMP_STATUS_BUSY " + Constants.PMP_STATUS_BUSY);
				// Run the OFFER Code by invoking the Crind offers.
				// pumpFlow.execute(msgInf.getID(), Constants.PMP_TRANSACTION_TOTAL);
				// pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
				submitPumpcommandProcess(msgInf, data, Constants.PUMP_BUSY);
				// pumpFlow.execute(msgInf.getID(), Constants.PUMP_BUSY);
				break;
			case PumpMessage.PMP_STATUS_PRESET_EOT:
				// pumpFlow.execute(data[0], Constants.PMP_STATUS_PRESET_EOT);
				logger.info(" PMP_STATUS_PRESET_EOT " + Constants.PMP_STATUS_PRESET_EOT);
				// pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
				// pumpFlow.execute(msgInf.getID(), Constants.EOT_STATUS);
				submitPumpcommandProcess(msgInf, data, Constants.EOT_STATUS);
				// Change the Crind Msg.
				break;
			case PumpMessage.PMP_STATUS_PUMP_STOP:
				// pumpFlow.execute(data[0], Constants.PUMP_ONLINE);
				logger.info(" PMP_STATUS_PUMP_STOP " + PumpMessage.PMP_STATUS_PUMP_STOP);
				break;
			case PumpMessage.PMP_STATUS_SEND_DATA:
				logger.info(" PMP_STATUS_SEND_DATA " + Constants.PMP_STATUS_SEND_DATA);
				break;
			case PumpMessage.PMP_STATUS_NOOP:
				logger.info(" PMP_STATUS_NOOP " + PumpMessage.PMP_STATUS_NOOP);
				break;
			case PumpMessage.PMP_STATUS_AUTHORIZED:
				logger.info(" PMP_STATUS_AUTHORIZED " + PumpMessage.PMP_STATUS_AUTHORIZED);
				//pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
				//pumpFlow.execute(data[0], Constants.PUMP_AUTHORIZED);
				submitPumpcommandProcess(msgInf, data, Constants.PUMP_AUTHORIZED);
				break;
			case PumpMessage.PMP_STATUS_FILLUP_EOT:
				logger.info(" PMP_STATUS_FILLUP_EOT " + PumpMessage.PMP_STATUS_FILLUP_EOT);
				//pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
				//pumpFlow.execute(msgInf.getID(), Constants.EOT_STATUS);
				submitPumpcommandProcess(msgInf, data, Constants.EOT_STATUS);
				break;
			default:
				logger.info(" " + SerialIOMsgUtil.MSG_ERR_UNKNOWN_DEV);
				break;

			}
		}

	}
	

	private void submitPumpcommandProcess(final GilbMessageInf msgInf, final byte[] data, String command) {
		try {

			Runnable commandtask = () -> {
				Thread.currentThread().setName("commandProcess_" + command + data[0]);
				logger.info("inside submitcommandProcess " + command);
				if (flag) {
					processPumpCommand(msgInf, data, command);
				} else {
					ControlFlow pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
					pumpFlow.execute(data[0], command);
				}
			};

			FCExecuterService.executeTask(commandtask);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void processPumpCommand(GilbMessageInf msgInf, byte[] data, String command) {
		logger.info(" State machine flag ??? " + flag + " command: " + command);
		if (flag) {
			try {
				logger.info("inside processcommand : " + data[0] + " ,command: " + command);
				
				TranslatorServer.sendMessage(Constants.FORECORECLIENT, msgInf);
				/*dispenser.setCommandData(msgInf);
				dispenser.setState(command);
				dispenser.updateDispenser();
				dispenser.execute();*/
			} catch (Exception exp) {
				logger.error(" Exception in moving next ", exp);
			}
		} else {
			ControlFlow crindFlow;
			crindFlow = ControlFlowFactory.getControlType(SerialIOConfig.CRIND_PORT_TYPE);
			crindFlow.setMessageInf(msgInf);
			logger.info("inside processcommand : " + data[0] + " ,command: " + command);
			crindFlow.execute(data[0], command);
		}

	}

	private void submitEOTPrintTask(final GilbMessageInf msgInf, byte[] data, String command) {
		try {
			// processEOTPrint(msgInf);
			FCExecuterService.executeTask(new Thread() {
				@Override
				public void run() {
					setName("EOTPrint_" + msgInf.getID());
					logger.info("inside submitEOTPrintTask");
					if (flag) {
						TranslatorServer.sendMessage(Constants.PAYMENTCLIENT, msgInf);
						TranslatorServer.sendMessage(Constants.FORECORECLIENT, msgInf);

						//int id = ProtocolUtilities.getByteToInt(msgInf.getID());
						/*Dispenser dispenser = DispenserApp.pumStationMap.get(id);
						dispenser.setCommandData(msgInf);
						dispenser.setState(command);
						dispenser.updateDispenser();
						dispenser.execute();*/
					} else {
						processEOTPrint(msgInf, data, command);
					}

				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void processEOTPrint(final GilbMessageInf msgInf,  byte[] data, String command) {
		int devId;
		ControlFlow pumpFlow = ControlFlowFactory.getControlType(SerialIOConfig.PUMP_PORT_TYPE);
		logger.info(msgInf.getMessageName() + " :$%$$%$ EOT_TOTALS_RESP: " + msgInf.getBytes());
		devId = ProtocolUtilities.getByteToInt(msgInf.getID());

		PumpEOTTotalsMessage msgObj = new PumpEOTTotalsMessage(msgInf.getID());
		// (PumpEOTTotalsMessage)msgInf;

		try {
			msgObj.setBytes(msgInf.getBytes());
			logger.info(" #@$@#$@#$@#$#@$:EOT_TOTALS_RESP " + msgObj.toHex());
			msgObj.parseMessage();
			logger.info(msgObj.toString() + " : PumpEOTTotalsMessage ");
		} catch (Exception exp) {
			logger.error("Parsing message EOT_TOTALS_RESP Exception ", exp);
		}
		logEOTTotal(msgObj);
		//DispenserApp.pumStationMap.get(devId).setPumpTransaction(msgObj.getTransactionTotals());
		logger.info(devId + " : getTransactionTotals : " + msgObj.getTransactionTotals());
		
		
		ControlFlow crindFlow = ControlFlowFactory.getControlType(SerialIOConfig.CRIND_PORT_TYPE);		
		crindFlow.execute(msgInf.getID(), Constants.PRINT);

	}

	private void logEOTTotal(final PumpEOTTotalsMessage msgObj) {

		try {
			final PumpTransaction totalObj = msgObj.getTransactionTotals();
			Runnable kinesislogtask = () -> {
				try {
					logger.info(" log totalObj.toString() : " + totalObj.toString());
					//KinesisClient.postMessage(totalObj.toString(),							FuelControllerProperties.getAppPropsWithKey("kStreamName"));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error("Exception in KinesisClient postMessage", e);
				}
			};
			FCExecuterService.executeTask(kinesislogtask);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error("Exception in logEOTTotal", e);
		}
	}

	private void invokeCrindContoller(GilbMessageInf msgInf) {
		byte[] data = msgInf.getBytes();
		if (data.length < 4) {
			logger.info(" Invalid message. Ignoring the message ");
		}
		logger.info(" Inside invokeCrindContoller ..... " + ProtocolUtilities.FormatHexDump(data, data.length));
		int status = -1;
		try {
			int devId = data[3]; // Get Crind device of rcvd msg
			byte devId_b = data[3]; // Get Crind device of rcvd msg
			ControlFlow crindFlow = null;
			logger.info("  devId : " + devId);
		/*	switch (devId) { eg :
			case SerialIOMsgUtil.CMD_ACK:
				// processCMDACK(msgInf, data);
				// submitCardData(msgInf, data);
				submitcommandProcess(msgInf, data, Constants.CMD_ACK);
				// Need to find the correct placeholder
				// invokePumpContoller( msgInf);
				break;
			case SerialIOMsgUtil.CMD_NAK:
				submitcommandProcess(msgInf, data, Constants.CMD_NAK);
				break;
			case SerialIOMsgUtil.ONLINE:
				submitcommandProcess(msgInf, data, Constants.ONLINE);
				// Need to find the correct placeholder
				invokePumpContoller(msgInf);

				break;
			case SerialIOMsgUtil.OFFLINE:
				//crindFlow = ControlFlowFactory.getControlType(SerialIOConfig.CRIND_PORT_TYPE);
				//crindFlow.setMessageInf(msgInf);
				//crindFlow.execute(data[0], Constants.OFFLINE);
				submitcommandProcess(msgInf, data, Constants.OFFLINE);
				break;
			case SerialIOMsgUtil.DEV_CRIND:
				status = SerialIOMsgUtil.MSG_DEV_CRIND;
				break;
			case SerialIOMsgUtil.DEV_BEEPER: // Crind never sends Beeper msg
				status = SerialIOMsgUtil.MSG_DEV_BEEPER;
				break;
			case SerialIOMsgUtil.DEV_DISPLAY: // Crind never sends Display msg
				status = SerialIOMsgUtil.MSG_DEV_DISPLAY;
				break;
			case SerialIOMsgUtil.DEV_CARDREADER:
				submitcommandProcess(msgInf, data, Constants.CRINDDATA);
				break;
			case SerialIOMsgUtil.DEV_KEYBOARD: // Crind never sends Keypad msg
				// submitKeyboardData(msgInf, data);
				submitcommandProcess(msgInf, data, Constants.KEYBOARDDATA);
				break;
			case SerialIOMsgUtil.DEV_PRINTER:
				submitcommandProcess(msgInf, data, Constants.PRINTERDATA);
				break;
			default:
				status = SerialIOMsgUtil.MSG_ERR_UNKNOWN_DEV;
				break;
			} */
		} catch (Exception Error) {
			logger.info("Exception error in parseRcvdMsg." + Error.toString());
			Error.printStackTrace();
		}

	}
	
	private void processEvents(GilbMessageInf msgInf, byte[] data, String command) {
		
		if (command.equalsIgnoreCase(Constants.CRINDDATA) ) {
			TranslatorServer.sendMessage(Constants.PAYMENTCLIENT, msgInf);
			//Need to process for CRINDDATA Message and null the card data before sending to Core.
			TranslatorServer.sendMessage(Constants.FORECORECLIENT, msgInf);
		} else if (command.equalsIgnoreCase(Constants.KEYBOARDDATA)) {			
			TranslatorServer.sendMessage(Constants.PAYMENTCLIENT, msgInf);
			TranslatorServer.sendMessage(Constants.FORECORECLIENT, msgInf);
		}
		else {
			TranslatorServer.sendMessage(Constants.FORECORECLIENT, msgInf);
			
		}
	
	}

	private void processCommand(GilbMessageInf msgInf, byte[] data, String command) {
		logger.info(" State machine flag ??? " + flag + " command: " + command);
		if (flag) {
			try {
				logger.info("inside processcommand : " + data[0] + " ,command: " + command);
				int id = ProtocolUtilities.getByteToInt(msgInf.getID());
				/*Dispenser dispenser = DispenserApp.pumStationMap.get(id);
				dispenser.setCommandData(msgInf);
				dispenser.setState(command);
				dispenser.updateDispenser();
				dispenser.execute();*/
			} catch (Exception exp) {
				logger.error(" Exception in moving next ", exp);
			}
		} else {
			ControlFlow crindFlow;
			crindFlow = ControlFlowFactory.getControlType(SerialIOConfig.CRIND_PORT_TYPE);
			crindFlow.setMessageInf(msgInf);
			logger.info("inside processcommand : " + data[0] + " ,command: " + command);
			crindFlow.execute(data[0], command);
		}
	}

	private void submitcommandProcess(final GilbMessageInf msgInf, final byte[] data, String command) {
		try {

			Runnable commandtask = () -> {
				Thread.currentThread().setName("commandProcess_" + command + data[0]);
				logger.info("inside submitcommandProcess " + command);
				
				//processCommand(msgInf, data, command);
				processEvents(msgInf, data, command);
			};

			FCExecuterService.executeTask(commandtask);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void submitKeyboardData(final GilbMessageInf msgInf, final byte[] data) {
		try {
			Thread keyboardThread = new Thread() {

				@Override
				public void run() {
					processKeyboardData(msgInf, data);
				}

			};
			keyboardThread.setName("keyboardProcess_" + data[0]);
			FCExecuterService.executeTask(keyboardThread);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void processKeyboardData(final GilbMessageInf msgInf, final byte[] data) {
		ControlFlow crindFlow = ControlFlowFactory.getControlType(SerialIOConfig.CRIND_PORT_TYPE);
		crindFlow.setMessageInf(msgInf);
		crindFlow.execute(data[0], Constants.KEYBOARDDATA);
	}

	private void submitCardData(final GilbMessageInf msgInf, final byte[] data) {
		try {
			FCExecuterService.executeTask(new Thread() {
				@Override
				public void run() {
					this.setName("CardData_" + data[0]);
					processCardData(msgInf, data);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void processCardData(GilbMessageInf msgInf, byte[] data) {
		ControlFlow crindFlow = ControlFlowFactory.getControlType(SerialIOConfig.CRIND_PORT_TYPE);
		crindFlow.setMessageInf(msgInf);
		crindFlow.execute(data[0], Constants.CRINDDATA);
	}

	public Integer getPortType() {
		return portType;
	}

	public void setPortType(Integer portType) {
		this.portType = portType;
	}

	public static void main(String[] args) {
		RecMessageQueueForApps recqueInstance1 = RecMessageQueueForApps.getInstance();
		String data = "3332206c42343736313733393030313031303031305e544553542f4341524420303320202020202020202020202020205e323231323230313131373538393030353430303030303030343032363332382045343736313733393030313031303031303d32323132323031313134333837383030313632390321";
		System.out.println(data.trim());
		GilbMessageInf crndMsg = new CrindMessage((byte) 8, CrindMessage.RESPONSE_MSG);
		byte[] byteArr = StringToByteArray(data.trim());
		System.out.println(byteArr.length);
		System.out.println(byteArr);
		for (int i = 0; i < byteArr.length; i++) {
			System.out.print((char) byteArr[i]);
		}
		System.out.println("");
		System.out.println((byteArr[0] == CrindMessage.CRIND_DEVICE_CARD_READER) + " >> " + byteArr[0]);
		byteArr = formatResMsg((byte) 8, 0, byteArr);
		System.out.println("");
		System.out.println(byteArr.length);

		System.out.println(ProtocolUtilities.FormatHexDump(byteArr, byteArr.length));
		for (int i = 0; i < byteArr.length; i++) {
			System.out.print((char) byteArr[i]);
		}
		crndMsg.setBytes(byteArr);
		crndMsg.setID(byteArr[0]);

		recqueInstance1.setMessage(crndMsg, SerialIOConfig.CRIND_PORT_TYPE);

	}

	private static byte[] formatResMsg(byte id, int pos, byte[] byteArr) {
		byte[] byteArrRes = new byte[byteArr.length + 3];
		int i = 0;
		byteArrRes[i] = id;
		i++;
		byteArrRes[i] = (byte) pos;
		i++;
		byteArrRes[i] = (byte) byteArr.length;
		i++;
		System.arraycopy(byteArr, 0, byteArrRes, i, byteArr.length);
		return byteArrRes;
	}

	public static byte[] StringToByteArray(String hex) {
		int NumberChars = hex.length();
		byte[] bytes = new byte[NumberChars / 2];
		for (int i = 0; i < NumberChars; i = i + 2)
			bytes[i / 2] = Byte.parseByte(hex.substring(i, i + 2), 16);

		return bytes;
	}

}
