package com.sevenEleven.dispenser.message;

public interface ControlFlow {
	public void execute(byte crindID, String operation);
	public void setMessageInf(GilbMessageInf inf);
	//public void execute(GenericDispenser genericDispenser);

}
