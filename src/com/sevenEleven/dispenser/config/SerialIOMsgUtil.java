package com.sevenEleven.dispenser.config;

public interface SerialIOMsgUtil {

	// =====================================================================
	// Control Message Bytes
	public static final byte ACK = 0x06;
	public static final byte NAK = 0x15;
	public static final byte STX = 0x02;
	public static final byte ETX = 0x03;
	public static final byte SOH = 0x01;
	public static final byte ENQ = 0x05;
	public static final byte EOT = 0x04;
	public static final byte CAN = 0x18;

	// =====================================================================
	// Standard Timeout/Retries
	public static final int MAX_RESPONSE_TIMEOUT = 250;
	public static final int MAX_INTERCHAR_TIMEOUT = 50;
	public static final int DEF_SEND_RETRIES = 3;

	// ====================================================================
	// Messaging Ids (message counts)
	public static final byte BROADCASTID = 0x01;
	public static final byte POLLID_START = 0x40;
	public static final byte POLLID_MAX = 0x5F;
	public static final byte SELECTID_START = 0x60;
	public static final byte SELECTID_MAX = 0x7F;
	public static int maxNumSIDs = SELECTID_MAX - SELECTID_START + 1;
	public static byte[] CrindSID = new byte[maxNumSIDs];

	// =====================================================================
	public static final int MSG_OK = 0;
	public static final int MSG_ERR_INVALID_LRS = -1;
	public static final int MSG_ERR_MISSING_ETX = -2;
	public static final int MSG_ERR_TOO_LONG = -3;
	public static final int MSG_ERR_MISSING_LRS = -4;
	public static final int MSG_ERR_PARTIAL_MSG = -5;
	public static final int MSG_ERR_UNFRAMED_MSG = -6;
	public static final int MSG_ERR_ETX_ENDFRAME = -7;
	public static final int MSG_ERR_FRAME_ERROR = -8;
	public static final int MSG_ERR_TIMEOUT = -9;
	public static final int MSG_ERR_NO_RCVD_DATA = -10;
	public static final int MSG_ERR_QUERY_RCV_BUF = -11;
	public static final int MSG_ERR_SINGLE_BYTE = -12;
	public static final int MSG_ERR_UNKNOWN_DEV = -20;

	public static final int MSG_RCVD_EOT = 0x100;
	public static final int MSG_RCVD_ACK = 0x101;
	public static final int MSG_RCVD_NAK = 0x102;
	public static final int MSG_RCVD_CAN = 0x103;
	public static final int MSG_RCVD_UNKNOWN = 0x104;

	public static final int MSG_DEV_CRIND = 0x200;
	public static final int MSG_DEV_BEEPER = 0x201;
	public static final int MSG_DEV_DISPLAY = 0x202;
	public static final int MSG_DEV_CARDREADER = 0x203;
	public static final int MSG_DEV_KEYBOARD = 0x204;
	public static final int MSG_DEV_PRINTER = 0x205;

	// ====================================================================

	// Pump Hardware Devices
	public static final byte DEV_CRIND = 0x30;
	public static final byte DEV_BEEPER = 0x31;
	public static final byte DEV_DISPLAY = 0x32;
	public static final byte DEV_CARDREADER = 0x33;
	public static final byte DEV_KEYBOARD = 0x34;
	public static final byte DEV_PRINTER = 0x35;
	public static final int ONLINE = 1; //4
	public static final int OFFLINE = -1;
	public static final int CMD_ACK = 4;
	public static final int CMD_NAK = 5;

	// ====================================================================
	
	public static boolean SENDFLAG = true;
	public static boolean READFLAG = false;
	
}
