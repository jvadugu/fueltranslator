package com.sevenEleven.dispenser.config;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sevenEleven.dispenser.crind.CrindMessage;

public class SerialIOConfig {
	
	public static int		pumpPollDelay = 50;	// milliseconds
	public static int		crindPollDelay = 50; // milliseconds
	public static int		crindTransmitDelay = 50; // milliseconds
	public static int		numDefaultPolls = 2;
	public static boolean	usePumpSim = false;
	public static boolean	useCallback = false;
	public static String	pumpPortName = "COM5";
	public static String	crindPortName = "COM6";
	public static String	epsPortName = "COM7";
	public static byte		startID = 8, endID = 8;
	public static String[]	commPortNames = {};
	
	public static final int	PUMP_PORT_TYPE 		= 0;
	public static final int CRIND_PORT_TYPE 	= 1;
	public static final int EPS_PORT_TYPE		= 2;
	public static final int SERVER_PORT_TYPE    = 3;
	
	public static final int PAYMENT_PORT_TYPE				= 5;
	public static final int CORE_PORT_TYPE				    = 6;
	
	public static final int MAX_CRIND_COUNT	= 32;
	private static final Logger logger = LogManager.getLogger(SerialIOConfig.class.getName());
	
	// Member variables
		public static byte[] ctrlMSN = new byte[MAX_CRIND_COUNT];
		public static byte[] crindMSN = new byte[MAX_CRIND_COUNT];
		
		public static byte getCrindMSN(byte id) { return crindMSN[id-1]; }
		public static void setCrindMSN(byte id, byte msn) { crindMSN[id-1] = msn; }
		public static byte getCtrlMSN(byte id) { return ctrlMSN[id-1]; }
		public static byte getNextMSN(int id) {
			
			logger.info("current msn : "+ctrlMSN[id-1]);
			if (ctrlMSN[id-1] < CrindMessage.CRIND_MAX_TX_MSN) {
				++ctrlMSN[id-1];
				byte x = ctrlMSN[id-1];
				logger.info(ctrlMSN[id-1]+" : after incer++ msn : "+x);
				if (x < CrindMessage.CRIND_MAX_TX_MSN)
						return x;
				else {
					ctrlMSN[id-1] = CrindMessage.CRIND_MIN_TX_MSN;
				}
				logger.info(ctrlMSN[id-1]+" : latest msn : "+x);
				return ctrlMSN[id-1];
			} else {
				return ctrlMSN[id-1] = CrindMessage.CRIND_MIN_TX_MSN;
			}
		}


}
