package com.sevenEleven.dispenser.config;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sevenEleven.dispenser.message.GilbMessageInf;

public class RecMessageQueue {
	
	private static RecMessageQueue instance = new RecMessageQueue();
	private LinkedBlockingQueue<GilbMessageInf> crindQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private LinkedBlockingQueue<GilbMessageInf> pumpQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private LinkedBlockingQueue<GilbMessageInf> epsQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private LinkedBlockingQueue<GilbMessageInf> agentRecQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private  boolean SENDFLAG = true;
	private static final Logger logger = LogManager.getLogger(RecMessageQueue.class.getName());
	
	

	/**
	 * @return the instance
	 */
	public static RecMessageQueue getInstance() {
		return instance;
	}
	
	public void setMessage(GilbMessageInf msg, int type) {
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: crindQueue.offer(msg);			
			break;
		case SerialIOConfig.PUMP_PORT_TYPE: pumpQueue.offer(msg);
			break;
		case SerialIOConfig.EPS_PORT_TYPE:epsQueue.offer(msg);
			break;
		case SerialIOConfig.SERVER_PORT_TYPE:agentRecQueue.offer(msg);
		break;
		default:
				logger.info("unable to push message to Queue :"+msg+" type:"+type);
		}
		
	}
	
	public GilbMessageInf getMessage(int type) {
		GilbMessageInf gilbMessageInf = null;
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: 
			gilbMessageInf = crindQueue.poll();			
			break;
		case SerialIOConfig.PUMP_PORT_TYPE: 
			gilbMessageInf = pumpQueue.poll();
			break;
		case SerialIOConfig.EPS_PORT_TYPE: 
			gilbMessageInf = epsQueue.poll();
			break;
		case SerialIOConfig.SERVER_PORT_TYPE:
			gilbMessageInf = agentRecQueue.poll();
		break;
		default:
		}
		return gilbMessageInf;
	}
	
	public GilbMessageInf peek(int type) {
		GilbMessageInf gilbMessageInf = null;
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: 
			gilbMessageInf = crindQueue.peek();			
			break;
		case SerialIOConfig.PUMP_PORT_TYPE: 
			gilbMessageInf = pumpQueue.peek();
			break;
		case SerialIOConfig.EPS_PORT_TYPE: 
			gilbMessageInf = epsQueue.peek();
			break;
		case SerialIOConfig.SERVER_PORT_TYPE:
			gilbMessageInf = agentRecQueue.peek();
		break;
		default:
		}
		return gilbMessageInf;
	}
	public boolean isEmpty(int type) {
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: 
			return crindQueue.isEmpty();			
		case SerialIOConfig.PUMP_PORT_TYPE: 
			return pumpQueue.isEmpty();
		case SerialIOConfig.EPS_PORT_TYPE: 
			return epsQueue.isEmpty();
		case SerialIOConfig.SERVER_PORT_TYPE:
			return agentRecQueue.isEmpty();
		default:
				logger.info("unable to empty message to Queue : type:"+type);
		}
		return false;
	}

	/**
	 * @return the sENDFLAG
	 */
	public  boolean isSENDFLAG() {
		return SENDFLAG;
	}

	/**
	 * @param sENDFLAG the sENDFLAG to set
	 */
	public void setSENDFLAG(boolean sENDFLAG) {
		SENDFLAG = sENDFLAG;
	}

	
	

}
