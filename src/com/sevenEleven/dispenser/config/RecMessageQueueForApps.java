package com.sevenEleven.dispenser.config;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sevenEleven.dispenser.message.GilbMessageInf;

public class RecMessageQueueForApps {
	
	private static RecMessageQueueForApps instance = new RecMessageQueueForApps();
	private LinkedBlockingQueue<GilbMessageInf> paymentQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private LinkedBlockingQueue<GilbMessageInf> controllerQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private LinkedBlockingQueue<GilbMessageInf> epsQueue = new LinkedBlockingQueue<GilbMessageInf>();
	private  boolean SENDFLAG = true;
	private static final Logger logger = LogManager.getLogger(SendMessageQueue.class.getName());
	
	/**
	 * @return the instance
	 */
	public static RecMessageQueueForApps getInstance() {
		return instance;
	}
	
	public void setMessage(GilbMessageInf msg, int type) {
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: paymentQueue.offer(msg);			
			break;
		case SerialIOConfig.PUMP_PORT_TYPE: controllerQueue.offer(msg);
			break;
		case SerialIOConfig.EPS_PORT_TYPE:epsQueue.offer(msg);
			break;
		default:
				logger.info("unable to push message to Queue :"+msg+" type:"+type);
		}
		
	}
	
	public GilbMessageInf getMessage(int type) {
		GilbMessageInf gilbMessageInf = null;
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: 
			gilbMessageInf = paymentQueue.poll();			
			break;
		case SerialIOConfig.PUMP_PORT_TYPE: 
			gilbMessageInf = controllerQueue.poll();
			break;
		case SerialIOConfig.EPS_PORT_TYPE: 
			gilbMessageInf = epsQueue.poll();
			break;
		default:
				logger.info("unable to poll message to Queue : type:"+type);
		}
		return gilbMessageInf;
	}
	
	public boolean isEmpty(int type) {
		switch(type) {
		case SerialIOConfig.CRIND_PORT_TYPE: 
			return paymentQueue.isEmpty();			
		case SerialIOConfig.PUMP_PORT_TYPE: 
			return controllerQueue.isEmpty();
		case SerialIOConfig.EPS_PORT_TYPE: 
			return epsQueue.isEmpty();
		default:
				logger.info("unable to empty message to Queue : type:"+type);
		}
		return false;
	}
	
	/**
	 * @return the sENDFLAG
	 */
	public  boolean isSENDFLAG() {
		return SENDFLAG;
	}

	/**
	 * @param sENDFLAG the sENDFLAG to set
	 */
	public void setSENDFLAG(boolean sENDFLAG) {
		SENDFLAG = sENDFLAG;
	}

}
