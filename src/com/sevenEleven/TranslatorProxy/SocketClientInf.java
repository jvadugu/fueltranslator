package com.sevenEleven.TranslatorProxy;

import com.sevenEleven.dispenser.message.GilbMessageInf;

public interface SocketClientInf {

	public void sendMessage(GilbMessageInf obj , String clientType);
	
}
