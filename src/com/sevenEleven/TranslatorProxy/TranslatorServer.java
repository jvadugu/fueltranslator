package com.sevenEleven.TranslatorProxy;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sevenEleven.dispenser.message.GilbMessageInf;


public class TranslatorServer {
	public static Map<Integer, SocketClientInf> FCCorePaymentMap = new HashMap<Integer, SocketClientInf>();
	private static final Logger logger = LogManager.getLogger("AgentServer");
	
	
	public static void setFuelAgentClient(int key, SocketClientInf client) {
		if (!FCCorePaymentMap.containsKey(key)) {
			FCCorePaymentMap.put(key, client);
		}
		
	}
	
	public static void sendMessage(int key, GilbMessageInf obj) {
		if (FCCorePaymentMap.containsKey(key)) {
			SocketClientInf client = FCCorePaymentMap.get(key);
			logger.info("Before sending message--"+obj);
			client.sendMessage(obj, ""+key);
		}
		
	}

	public static void sendMessageToAll(GilbMessageInf obj) {
		for (Integer key : FCCorePaymentMap.keySet()) {	
			SocketClientInf client = FCCorePaymentMap.get(key);
			client.sendMessage(obj, ""+key);
		}
		
	}
}